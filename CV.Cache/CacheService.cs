﻿using CV.Cache;
using System;
using System.Runtime.Caching;

namespace CV.Cache
{
    public class CacheService : ICacheService
    {
        /// <summary>
        /// Returns cached item of data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="getItemCallback"></param>
        /// <param name="cacheDuration"></param>
        /// <returns></returns>
        public T GetOrSet<T>(string cacheKey, Func<T> getItemCallback, DateTimeOffset cacheDuration) where T : class
        {
            return OrSet(cacheKey, getItemCallback, cacheDuration);
        }

        /// <summary>
        /// Returns cached item of data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="getItemCallback">2 days</param>
        /// <returns></returns>
        public T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            var cacheDuration = DateTime.Now.AddDays(2);

            return OrSet(cacheKey, getItemCallback, cacheDuration);
        }

        public void DeleteCacheEntry(string cacheKey)
        {
            MemoryCache.Default.Remove(cacheKey);
        }

        private static T OrSet<T>(string cacheKey, Func<T> getItemCallback, DateTimeOffset cacheDuration) where T : class
        {
            var item = MemoryCache.Default.Get(cacheKey) as T;

            if (item != null)
                return item;

            item = getItemCallback();

            if (item == null)
                return null;

            MemoryCache.Default.Add(cacheKey, item, cacheDuration);
            return item;
        }
    }
}