﻿using System;

namespace CV.Cache
{
    public interface ICacheService
    {
        T GetOrSet<T>(string cacheKey, Func<T> getItemCallback, DateTimeOffset cacheDuration) where T : class;
        T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class;
        void DeleteCacheEntry(string cacheKey);

    }
}