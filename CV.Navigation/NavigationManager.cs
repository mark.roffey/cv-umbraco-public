﻿using CV.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CV.Navigation.ViewModels;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Zone.UmbracoMapper.V8;

namespace CV.Navigation
{
    public class NavigationManager : INavigationManager
    {
        private IUmbracoContextFactory _context;
        private UmbracoHelper _umbracoHelper;

        public NavigationManager(IUmbracoContextFactory context, UmbracoHelper helper)
        {
            _context = context;
            _umbracoHelper = helper;
        }

        public Home GetHomePage()
        {
            var homepage = _umbracoHelper.ContentAtRoot().ToList();
                
            if (!homepage.Any(x => x is Home))
                throw new NullReferenceException("Home Page not found");

            return homepage.First(x => x is Home) as Home;
        }


        public AboutMe GetAboutPage()
        {
            var aboutMe = this.GetHomePage().DescendantsOrSelf<AboutMe>().ToList();

            if (!aboutMe.Any())
                throw new NullReferenceException("About Me Page not found");

            return aboutMe.First();
        }
        public Contact GetContactPage()
        {
            var contact = this.GetHomePage().DescendantsOrSelf<Contact>().ToList();

            if (!contact.Any())
                throw new NullReferenceException("Contact Page not found");

            return contact.First();
        }

        public IEnumerable<Job> GetJobs()
        {
            var jobs = GetHomePage().Descendants<Job>();

            if (jobs.Any(x => x == null)) 
                throw new NullReferenceException("No jobs found");

            return jobs;
        }

        public IEnumerable<IPublishedContent> GetNavigationItems()
        {
            var navigation = GetHomePage().Descendants();

            if (navigation.Any(x => x == null))
                throw new NullReferenceException("No navigation items found");

            return navigation;
        }



        //public IPublishedContent GetCurrentPage()
        //{
        //  return _umbracoHelper.TypedContent(TopoGraph.Node<,>.getCurrentNodeId());
        //}
    }
}
