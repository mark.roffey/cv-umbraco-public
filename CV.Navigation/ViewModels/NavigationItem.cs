﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web.Mvc;

namespace CV.Navigation.ViewModels
{
    public class NavigationItem
    { 
        public string Name { get; set; }    
        public string Link { get; set; }
        public bool ShowOnHome{ get; set; }
    }
}
