﻿using System.Collections.Generic;
using CV.Models;
using CV.Navigation.ViewModels;
using Umbraco.Core.Models.PublishedContent;

namespace CV.Navigation
{
    public interface INavigationManager
    {
        Home GetHomePage();

        AboutMe GetAboutPage();
        Contact GetContactPage();
        IEnumerable<Job> GetJobs();

        IEnumerable<IPublishedContent> GetNavigationItems();

    }
}