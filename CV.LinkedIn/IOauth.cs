﻿namespace CV.LinkedIn
{
    public interface IOauth
    {
        string GetOauthToken();
    }
}