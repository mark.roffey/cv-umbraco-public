﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;

namespace CV.LinkedIn
{
    /// <summary>
    /// OBSOLETE - LINKED IN API IS NOT ACCEPTING NEW REQUESTS
    /// </summary>
    public class Oauth : IOauth
    {
        private readonly Configuration.LinkedIn.IConfigurationProperties _linkedInConfigurationManager;

        public Oauth(Configuration.LinkedIn.IConfigurationProperties linkedInConfigurationManager)
        {
            _linkedInConfigurationManager = linkedInConfigurationManager;
        }

        public string GetOauthToken()
        {
            const string url = "https://api.linkedin.com/";

            var restClient = new RestClient(url);

            var request = new RestRequest("oauth/v2/accessToken")
            {
                Method = Method.GET
            };

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("client_id", _linkedInConfigurationManager.LinkedInClientId());
            request.AddParameter("client_secret", _linkedInConfigurationManager.LinkedInClientSecret());
            request.AddParameter("grant_type", "client_credentials");

            var resp = restClient.Execute(request);
            var json = resp.Content;

            var token = JsonConvert.DeserializeObject<Dictionary<string, object>>(json)["access_token"].ToString();
                return token.Length > 0 ? token : null;
        }
    }
}
