﻿using System;
using System.Linq;
using CV.LinkedIn;
using System.Web.Mvc;
using CV.Cache;
using CV.Navigation;
using CV.Twitter;
using Tweetinvi.Core.Extensions;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CV.Umbraco.V8.SurfaceControllers
{
    public class HomePageController : SurfaceController
    {
        private readonly INavigationManager _navigationManager;
        private readonly ITwitter _twitter;
        private readonly ICacheService _cacheService;

        public HomePageController(INavigationManager navigationManager, ITwitter twitter, ICacheService cacheService)
        {
            _navigationManager = navigationManager;
            _twitter = twitter;
            _cacheService = cacheService;
        }

        // GET: LinkedIn
        [ChildActionOnly]


        public ActionResult Experience()
        {
            var experience = _navigationManager.GetJobs().OrderByDescending(x => x.StartDate).Take(3);
            return PartialView("Experience", experience);
        }

        // GET: AboutMe
        [ChildActionOnly]
        public ActionResult AboutMe()
        {
            var aboutMe = _navigationManager.GetAboutPage();
            return PartialView("AboutMe", aboutMe);
        }

        // GET: Contact
        [ChildActionOnly]
        public ActionResult Contact()
        {
            var contact = _navigationManager.GetContactPage();
            return PartialView("Contact", contact);
        }

        [ChildActionOnly]
        public ActionResult Links()
        {
            var homePage = _navigationManager.GetHomePage();
            var myLinks = homePage.MyLinks;
            return PartialView("Links", myLinks);
        }

        [ChildActionOnly]
        public ActionResult Tweets()
        {
            //var search = "\"Umbraco\" (job OR jobs OR hiring)";
            var search = "Umbraco developer job";
            var cacheKey = $"twitter-{search}";
            var dateFrom = DateTime.Now.AddDays(-7);

            var allTweets = _cacheService.GetOrSet(cacheKey, () => _twitter.SearchTwitter(search));


            var tweets = allTweets.Where(x => x.IsRetweet == false)
                .Where(x => x.CreatedAt > dateFrom);

            return PartialView("Tweets", tweets.Take(6));
        }
    }
}