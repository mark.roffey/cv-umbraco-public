﻿using System.Linq;
using CV.Navigation;
using System.Web.Mvc;
using CV.LinkedIn;
using CV.Twitter;
using Umbraco.Web.Mvc;

namespace CV.Umbraco.V8.SurfaceControllers
{
    public class ContactController : SurfaceController
    {
        private readonly INavigationManager _navigationManager;

        public ContactController(INavigationManager navigationManager, ITwitter twitter)
        {
            _navigationManager = navigationManager;
        }

        // GET: LinkedIn
        [ChildActionOnly]
        public ActionResult Index()
        {
          

            return PartialView("Contact.");
        }
    }
}