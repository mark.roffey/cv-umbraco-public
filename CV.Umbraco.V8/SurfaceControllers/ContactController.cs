﻿using System.Linq;
using CV.Navigation;
using System.Web.Mvc;
using CV.LinkedIn;
using CV.Twitter;
using Umbraco.Web.Mvc;

namespace CV.Umbraco.V8.SurfaceControllers
{
    public class GlobalController : SurfaceController
    {
        private readonly INavigationManager _navigationManager;

        public GlobalController(INavigationManager navigationManager, ITwitter twitter)
        {
            _navigationManager = navigationManager;
        }

        // GET: LinkedIn
        [ChildActionOnly]
        public ActionResult Navigation()
        {
            return PartialView("Contact.");
        }
    }
}