﻿using System.Linq;
using CV.LinkedIn;
using System.Web.Mvc;
using CV.Cache;
using CV.Navigation;
using CV.Twitter;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CV.Umbraco.V8.SurfaceControllers
{
    public class SiteController : SurfaceController
    {
        private readonly INavigationManager _navigationManager;

        public SiteController(INavigationManager navigationManager)
        {
            _navigationManager = navigationManager;
        }

        // GET: Navigation
        [ChildActionOnly]
        public ActionResult Navigation()
        {
            var navigationItems = _navigationManager.GetNavigationItems();
            return PartialView("Navigation", navigationItems.ToList());
        }
        
        public ActionResult MobileNavigation()
        {
            var navigationItems = _navigationManager.GetNavigationItems();
            return PartialView("MobileNavigation", navigationItems.ToList());
        }

    }
}