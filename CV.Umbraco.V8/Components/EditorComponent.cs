﻿using System.Linq;
using System.Web.Http.Filters;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;
using Umbraco.Web.Editors;
using Umbraco.Web.Models.ContentEditing;

namespace CV.Umbraco.V8.Components
{
    public class EditorComponent : IComponent
    {
        public void Initialize() =>
            EditorModelEventManager.SendingContentModel += EditorModelEventManager_SendingContentModel;

        private void EditorModelEventManager_SendingContentModel(HttpActionExecutedContext sender, EditorModelEventArgs<ContentItemDisplay> e)
        {
            if (e.Model.TemplateId != 0)
                return;

            e.Model.AllowPreview = false;
            e.Model.Urls = null;
        }

        public void Terminate() { }
    }

}