﻿using CV.LinkedIn;
using CV.Navigation;
using CV.Twitter;
using CV.Umbraco.V8.Components;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace CV.Umbraco.V8.Composer
{
    // Register your own services by implementing IUserComposer
    public class EditorComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components().Append<EditorComponent>();
        }
    }
}