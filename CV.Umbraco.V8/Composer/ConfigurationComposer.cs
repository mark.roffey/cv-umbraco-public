﻿using CV.Configuration;
using CV.LinkedIn;
using CV.Navigation;
using CV.Twitter;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace CV.Umbraco.V8.Composer
{
    // Register your own services by implementing IUserComposer
    public class ConfigurationComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<IConfiguration, Configuration.Configuration>();
            composition.Register<Configuration.Twitter.IConfigurationProperties, Configuration.Twitter.ConfigurationProperties>();
            composition.Register<Configuration.LinkedIn.IConfigurationProperties, Configuration.LinkedIn.ConfigurationProperties>();
        }
    }
}