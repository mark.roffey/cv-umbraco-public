﻿using CV.Cache;
using CV.LinkedIn;
using CV.Navigation;
using CV.Twitter;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace CV.Umbraco.V8.Composer
{
    // Register your own services by implementing IUserComposer
    public class CacheComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<ICacheService, CacheService>();
        }
    }
}