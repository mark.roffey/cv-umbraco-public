﻿using CV.LinkedIn;
using CV.Navigation;
using CV.Twitter;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace CV.Umbraco.V8.Composer
{
    // Register your own services by implementing IUserComposer
    public class NavigationComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<INavigationManager, NavigationManager>();
            composition.Register<IOauth, Oauth>();
            composition.Register<ITwitter, Twitter.Twitter>();
        }
    }
}