## https://makdns.blogspot.com/2017/11/powershell-read-appconfig.html

cls

$PROJECT = "CV.Umbraco.V8"

$scriptRoot = Split-Path $PSScriptRoot

### path of config file

$app_config_path = $scriptRoot + "\" + $PROJECT + "\web.config"
Write-Host  $app_config_path

### reading configuration file
$xml_doc = [Xml](Get-Content $app_config_path)

### get dbconnection setting
$con_nodes = $xml_doc.configuration.connectionStrings.add
$db_con = ($con_nodes | where {$_.name -eq 'umbracoDbDSN'}).connectionString

### display value
Write-Host  $db_con



$sb = New-Object System.Data.Common.DbConnectionStringBuilder
$sb.set_ConnectionString($db_con)
$Database = $sb.User-ID

### display value
Write-Host  "hi: " + $Database