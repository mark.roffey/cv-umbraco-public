﻿<#
	The sample scripts are not supported under any Microsoft standard support 
	program or service. The sample scripts are provided AS IS without warranty  
	of any kind. Microsoft further disclaims all implied warranties including,  
	without limitation, any implied warranties of merchantability or of fitness for 
	a particular purpose. The entire risk arising out of the use or performance of  
	the sample scripts and documentation remains with you. In no event shall 
	Microsoft, its authors, or anyone Else involved in the creation, production, or 
	delivery of the scripts be liable for any damages whatsoever (including, 
	without limitation, damages for loss of business profits, business interruption, 
	loss of business information, or other pecuniary loss) arising out of the use 
	of or inability to use the sample scripts or documentation, even If Microsoft 
	has been advised of the possibility of such damages

  Modified from https://gallery.technet.microsoft.com/scriptcenter/How-to-export-SQL-Server-10452c4e
#>

param (
    [string]$source = "release"
)

$ErrorActionPreference = 'Stop'

if ($source -eq "local" ) {
	write-output "The build is $source"
	$ServerInstance = "BWVMSSQL03"
	$DatabaseName = "UmbracoPublicWebsiteTest"
	$filename = "\Backup\backup-local.bacpac"
}elseif ($source -eq "dev" ) {
	write-output "The build is $source"
	$ServerInstance = "BWVMSSQL03"
	$DatabaseName = "UmbracoPublicWebsiteUAT"
	$filename = "\Backup\backup-dev.bacpac"
} 
else {
	$ServerInstance = "BWVMSSQL04"
	$DatabaseName = "UmbracoPublicWebsite"
	$filename = "\Backup\backup-production.bacpac"
	write-output "The build is production"
}

$Username = "UmbracoPublicWebsite"
$password = "UmbracoWebDb18"
$BackupFilePath = $PSScriptRoot + $filename

Try {
	$ToolPath = "C:\Program Files (x86)\Microsoft SQL Server\140\DAC\bin"
	$OldPath = Get-Location
	Set-Location $ToolPath
	.\SqlPackage.exe /a:Export /ssn:$ServerInstance /sdn:$DatabaseName /su:$Username /sp:$Password /tf:$BackupFilePath
	Set-Location $OldPath.Path
} Catch {
	Set-Location $OldPath.Path
	Throw $_
}