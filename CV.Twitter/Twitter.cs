﻿using System.Collections.Generic;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace CV.Twitter
{
    public class Twitter : ITwitter
    {
        private readonly Configuration.Twitter.IConfigurationProperties _twitterConfigurationManager;

        public Twitter(Configuration.Twitter.IConfigurationProperties twitterConfigurationManager)
        {
            _twitterConfigurationManager = twitterConfigurationManager;
        }

        public  ITwitterCredentials Oauth()
        {
            var credentials = Auth.SetUserCredentials(
                _twitterConfigurationManager.TwitterAPIKey(),
                _twitterConfigurationManager.TwitterAPISecretKey(),
                _twitterConfigurationManager.TwitterAPIAccessToken(),
                _twitterConfigurationManager.TwitterAPIAccessTokenSecret());

            return credentials;
        }

        //TODO Rewrite with a custom search parameter API
        /// <summary>
        /// OBSOLETE: Search tweet parameters does not work and is reliant on a paid subscription to twitter
        /// https://github.com/linvi/tweetinvi/issues/709
        /// </summary>
        /// <param name="search"></param>
        /// <param name="results"></param>
        /// <returns></returns>
        public IEnumerable<ITweet> SearchTwitter(string search, int results)
        {
            Auth.ApplicationCredentials = Oauth();
            var searchParameter = new SearchTweetsParameters(search)
            {
                TweetSearchType = TweetSearchType.OriginalTweetsOnly,
                //GeoCode = new GeoCode(-122.398720, 37.781157, 1, DistanceMeasure.Miles),
                Lang = LanguageFilter.English,
                // SearchType = SearchResultType.Popular,
                MaximumNumberOfResults = results,
                //Until = new DateTime(2015, 06, 02),
                //SinceId = 399616835892781056,
                //MaxId = 405001488843284480,
               // Filters = TweetSearchFilters.Images | TweetSearchFilters.Verified
            };

            return Search.SearchTweets(searchParameter);
        }

        public IEnumerable<ITweet> SearchTwitter(string search)
        {
            TweetinviConfig.CurrentThreadSettings.TweetMode = TweetMode.Extended;
            Auth.ApplicationCredentials = Oauth();
            return Search.SearchTweets(search);
        }
    }
}