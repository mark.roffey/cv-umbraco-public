﻿using System.Collections.Generic;
using Tweetinvi.Models;

namespace CV.Twitter
{
    public interface ITwitter
    {
        ITwitterCredentials Oauth();
        IEnumerable<ITweet> SearchTwitter(string search, int results);
        IEnumerable<ITweet> SearchTwitter(string search);
    }
}