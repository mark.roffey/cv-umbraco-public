﻿namespace CV.Configuration
{
    public interface IConfiguration
    {
        string GetConfigurationKey(string KeyName);
    }
}