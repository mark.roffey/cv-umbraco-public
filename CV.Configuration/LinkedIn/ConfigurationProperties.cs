﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV.Configuration.LinkedIn
{
    public class ConfigurationProperties : IConfigurationProperties
    {
        private readonly IConfiguration _configuration;

        public ConfigurationProperties(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string LinkedInClientId()
        {
            return _configuration.GetConfigurationKey("LinkedInClientId");
        }

        public string LinkedInClientSecret()
        {
            return _configuration.GetConfigurationKey("LinkedInClientSecret");
        }

    }
}
