﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV.Configuration.Twitter
{
    public interface IConfigurationProperties
    {
        string TwitterAPIKey();
        string TwitterAPISecretKey();
        string TwitterAPIAccessToken();
        string TwitterAPIAccessTokenSecret();
    }
}
