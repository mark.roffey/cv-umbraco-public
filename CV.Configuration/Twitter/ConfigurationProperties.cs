﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV.Configuration.Twitter
{
    public class ConfigurationProperties : IConfigurationProperties
    {

        private readonly IConfiguration _configuration;

        public ConfigurationProperties(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string TwitterAPIKey()
        {
            return _configuration.GetConfigurationKey("TwitterAPIKey");
        }

        public string TwitterAPISecretKey()
        {
            return _configuration.GetConfigurationKey("TwitterAPISecretKey");
        }

        public string TwitterAPIAccessToken()
        {
            return _configuration.GetConfigurationKey("TwitterAPIAccessToken");
        }

        public string TwitterAPIAccessTokenSecret()
        {
            return _configuration.GetConfigurationKey("TwitterAPIAccessTokenSecret");
        }

    }
}
