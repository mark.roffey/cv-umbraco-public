﻿using System;
using System.Configuration;
using System.Linq;

namespace CV.Configuration
{
    public class Configuration : IConfiguration
    {
        public string GetConfigurationKey(string keyName)
        {

            if (ConfigurationManager.AppSettings.AllKeys.Any(key => key == keyName))
            {
                return string.Format(ConfigurationManager.AppSettings[keyName]);
            }

            throw new Exception($"{keyName} does not exist.");
        }
    }
}